/* eslint-disable no-console */
'use strict'

const uuid = require('uuid')
const AWS = require('aws-sdk') // eslint-disable-line import/no-extraneous-dependencies
const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const _ = require('lodash')
const idf = require('./ids')


const dynamoDb = new AWS.DynamoDB.DocumentClient()

const SCREENURI = 'https://www.screener.in'

const fetch = async (id, points) => {
  const data = {}
  let options = {}
  let $ = null
  if (process.env['http_proxy']) {
    options.proxy = {
      host: '172.16.2.30',
      port: 8080
    }
  }
  try {
    await axios.get(`${SCREENURI}/company/${id}/`, options).then(res => {
      $ = cheerio.load(res['data'])
      points.map(p => {
        data[p.field] = $(p.selector).text()
        if (p.conv) {
          data[p.field] = p.conv(data[p.field])
        }
      })
    })
    return data
  } catch(err) {
    console.log(err)
    return {}
  }
} 

const fetchId = async (id) => {
  const fallId = '500002'
  const getN = (str) => { const val = (str.replace(/,/g, '').match(/[+-]?\d+(\.\d+)?/g)); return val ? parseFloat(val[0]) : null }
  const points = [
    {field:'name', selector: '#company-info > h1'},
    {field:'price', selector: '#main-area > section:nth-child(5) > ul > li:nth-child(2) > b', conv: getN},
    {field:'bookvalue', selector: '#main-area > section:nth-child(5) > ul > li:nth-child(4) > b', conv: getN},
    {field:'pe', selector: '#main-area > section:nth-child(5) > ul > li:nth-child(5) > b', conv: getN},
    {field:'roe', selector: '#main-area > section:nth-child(5) > ul > li:nth-child(8) > b', conv: getN},
    {field:'roce', selector: '#main-area > section:nth-child(5) > ul > li:nth-child(7) > b', conv: getN},
  ]
  let data = await fetch(id, points)
  if (_.isEmpty(data)) {
    data = await fetch(fallId, points)
  }
  return data
}


module.exports.fetchAll = async (event, context, callback) => {
  const timestamp = new Date().getTime()
  const ids = idf.ids
  const promises = ids.map(async (id, key) => {
    let data = null
    data = await fetchId(id)
    const params = {
      TableName: process.env.DYNAMODB_TABLE,
      Item: {
        id: uuid.v1(),
        companyId: id,
        ...data,
        createdAt: timestamp,
        updatedAt: timestamp,
      }
    }
    await dynamoDb.put(params, (error) => {
      // handle potential errors
      if (error) {
        console.error(error)
        callback(null, {
          statusCode: error.statusCode || 501,
          headers: {
            'Content-Type': 'text/plain',
            "Access-Control-Allow-Origin" : "*",
          },
          body: 'Couldn\'t create the todo item.',
        })
        return
      }
    })
  })
  await Promise.all(promises)
  const response = {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : "*",
    },
    body: {
      message: "Inserted items to dynamodb",
    }
  }
  callback(null, response)
}
